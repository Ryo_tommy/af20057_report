import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Food_Machine {
    private JButton tempuraButton;
    private JButton udonButton;
    private JButton ramenButton;
    private JButton BeefStewButton;
    private JButton gyozaButton;
    private JButton hamburgerButton;
    private JTextPane orderlist;
    private JButton checkOutButton;
    private JLabel Total;
    private JPanel food_panel;
    private JLabel yen;
    private JButton wahuuButton;
    private JButton chukaButton;
    private JButton yohuuButton;
    private JButton cleanButton;

    int sum=0;

    void order(String food,int money){
        String currentText = orderlist.getText();
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Comfirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            sum+=+money;
            orderlist.setText(currentText + "Order for "+food+" "+money+"yen\n");
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+food+"! It will be served as soon as possible.");
            yen.setText(+sum+"yen");
        }
    }

    void  wahuu(){
        clean();
        gyozaButton.setEnabled(false);
        ramenButton.setEnabled(false);
        BeefStewButton.setEnabled(false);
        hamburgerButton.setEnabled(false);
    }

    void chuka(){
        clean();
        udonButton.setEnabled(false);
        tempuraButton.setEnabled(false);
        BeefStewButton.setEnabled(false);
        hamburgerButton.setEnabled(false);
    }

    void yohuu(){
        clean();
        udonButton.setEnabled(false);
        gyozaButton.setEnabled(false);
        ramenButton.setEnabled(false);
        tempuraButton.setEnabled(false);
    }

    void clean(){
        tempuraButton.setEnabled(true);
        udonButton.setEnabled(true);
        gyozaButton.setEnabled(true);
        ramenButton.setEnabled(true);
        BeefStewButton.setEnabled(true);
        hamburgerButton.setEnabled(true);
    }

    void textClean(){
        orderlist.setText(null);
        yen.setText("yen");
        sum=0;
    }

    void check(){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to Checkout?",
                "Checkout Comfirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            if(sum==0) {
                JOptionPane.showMessageDialog(null, "Nothing ordered.");
                return;
            }
            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + sum + "yen.");
            orderlist.setText(null);
            yen.setText("yen");
            sum=0;
        }
    }

    public Food_Machine() {
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("Tempurainjapan-ikebukuro-dec312007.jpg")));
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("submain1.png")));
        BeefStewButton.setIcon(new ImageIcon(this.getClass().getResource("OIP.jpg")));
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("20151020-3.jpg")));
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("0.jpeg")));
        hamburgerButton.setIcon(new ImageIcon(this.getClass().getResource("R.jpg")));

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",100);
            }
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",240);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",330);
            }
        });
        BeefStewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("BeefStew",150);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",180);
            }
        });
        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               order("hamburger",400);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                check();
                clean();
            }
        });
        wahuuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               wahuu();
            }
        });
        chukaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               chuka();
            }
        });
        yohuuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                yohuu();
            }
        });
        cleanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clean();
                textClean();
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("Food_Machine");
        frame.setContentPane(new Food_Machine().food_panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

